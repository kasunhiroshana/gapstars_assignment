<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Chart</title>

		<style type="text/css">
            #container {
                min-width: 100%;
                max-width:  100%;
                height: 500px;
                margin: 0 auto
            }
		</style>
	</head>
	<body>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>


<?php

$csv = file_get_contents('data.json'); // Reading json data from the file
$json = json_decode($csv, true);

//Creating an array for the chart. Values will be added daily basis

$result = [];
$i = 0;
foreach ($json as $v) {
    if (!isset($result[$v["created_at"]])) {
        $result[$v['created_at']]['name'] = $v['created_at'];
        $result[$v['created_at']]['data'] = $v['onboarding_perentage'];

    } else {
        if (!is_array($result[$v['created_at']]['data'])) {
            $result[$v['created_at']]['data'] = [];
        }
        array_push($result[$v['created_at']]['data'], $v['onboarding_perentage']);
    }

    $i++;
}
$result = array_values($result);

?>

<div id="container"></div>

<script type="text/javascript">

    var data = <?php echo json_encode($result, JSON_PRETTY_PRINT) ?>;

    $(function () {

        var options = {
            chart: {
                renderTo: 'container',
                type: 'line',
                options3d: {
                    enabled: true,
                    alpha: 0,
                    beta: 0,
                    depth: 0,
                    viewDistance: 25
                }
            },
            title: {
                text: 'Temper Onboarding Flow Process Data'
            },

            yAxis: {
                min: 0,
                max: 100,
                tickInterval: 20
            },

            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },

            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    }
                }
            },
            series: [],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }

        };

            options.series = data;
            var chart = new Highcharts.Chart(options);
    });
</script>
	</body>
</html>
